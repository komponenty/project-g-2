package com.grandrentauto.data;

import java.io.Serializable;


public class DataBase implements Serializable{
  private Boolean toEdit;  

    /**
     * @return the toEdit
     */
    public Boolean getToEdit() {
        return toEdit;
    }

    /**
     * @param toEdit the toEdit to set
     */
    public void setToEdit(Boolean toEdit) {
        this.toEdit = toEdit;
    }
}
