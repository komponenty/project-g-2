package com.grandrentauto.data;

import java.util.Date;


public class Rental {
    private Integer id;
    private Car car;
    private Client client;
    private Date rentalDate;
    private Date startDate;
    private Date endDate;

    
    
    
    public Rental(int id,Car car, Client client, Date start, Date end){
        this.id = id;
        this.car = car;
        this.client = client;
        this.startDate = start;
        this.endDate = end;
    }
    /**
     * @return the car
     */
    public Car getCar() {
        return car;
    }

    /**
     * @param car the car to set
     */
    public void setCar(Car car) {
        this.car = car;
    }

    /**
     * @return the client
     */
    public Client getClient() {
        return client;
    }

    /**
     * @param client the client to set
     */
    public void setClient(Client client) {
        this.client = client;
    }

    /**
     * @return the rentalDate
     */
    public Date getRentalDate() {
        return rentalDate;
    }

    /**
     * @param rentalDate the rentalDate to set
     */
    public void setRentalDate(Date rentalDate) {
        this.rentalDate = rentalDate;
    }

    /**
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * @param startDate the startDate to set
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate the endDate to set
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }
    
    public String toString(){
        return "Wynajem auta " + car.getModel() +", " + car.getRegistrationNumber() + ". Dla " + client.getFirstName() +" " + client.getLastName();
    }
}
