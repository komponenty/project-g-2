package com.grandrentauto.db;

import com.grandrentauto.data.Car;
import static com.grandrentauto.db.Database.conn;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.stereotype.Service;


@Service
public class CarDb extends Database<Car>{

    @Override
    public Car getRow(ResultSet rs) throws Exception{
       return  new Car(rs.getInt("id"), rs.getString("MODEL"), rs.getString("REGISTRATION_NUMBER"));
    }


    public List<Car> getCars(){
        String sql = "SELECT ID,MODEL,REGISTRATION_NUMBER from car";
        try{
        return getList(sql);
        }
        catch(Exception ex){
            return new ArrayList<Car>();
        }
    }
    public void saveCar(Car car) throws Exception{
       String sql = "INSERT INTO car(MODEL,REGISTRATION_NUMBER) VALUES(?,?)";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, car.getModel());
        pstmt.setString(2, car.getRegistrationNumber());
        
        pstmt.execute();
    }

    Car getCar(int carId) {
        String sql = "SELECT ID,MODEL,REGISTRATION_NUMBER from car where id = " + carId;
        try {
            return getList(sql).get(0);
        } catch (Exception ex) {
            Logger.getLogger(CarDb.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void updateCar(Car car) throws Exception{
         String sql = "UPDATE car SET MODEL = ? ,REGISTRATION_NUMBER = ? where id = " + car.getId();
          PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setString(1, car.getModel());
        pstmt.setString(2, car.getRegistrationNumber());
        
        pstmt.execute();
    }

    public void deleteCar(int id) throws Exception{
         String sql = "DELETE FROM car WHERE id = " + id;
          PreparedStatement pstmt = conn.prepareStatement(sql);
        
        pstmt.execute();
    }
    
    List<Car> getAvailableCars(java.util.Date startDate, java.util.Date endDate) throws Exception{
        
        String sql = "SELECT * FROM car where id not in (select r.idcar from wypozyczalnia.rental r where (r.start > ? and r.start < ?) or (r.end > ? and r.end < ?))";
        PreparedStatement pstmt = conn.prepareStatement(sql);
        pstmt.setDate(1, new java.sql.Date(startDate.getTime()));
        pstmt.setDate(2, new java.sql.Date(endDate.getTime()));
        pstmt.setDate(3, new java.sql.Date(startDate.getTime()));
        pstmt.setDate(4, new java.sql.Date(endDate.getTime()));
        
        ResultSet rs = pstmt.executeQuery();
        List<Car> result = new ArrayList<Car>();
        while(rs.next()){
            result.add(getRow(rs));
        }
        return result;
        
    }
    
}
