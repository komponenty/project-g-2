package com.grandrentauto.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class Database<T extends Object> {

    static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
//	static final String DB_URL = "jdbc:mysql://localhost/wypozyczalnia";
	static final String DB_URL = "jdbc:mysql://rexor.pl/wypozyczalnia";

    static final String USER = "wypozyczalnia";
    static final String PASS = "wypozyczalnia1";


    protected static Connection conn;
    protected Statement stmt;
    public Database(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
             
            if(conn == null){
             conn = DriverManager.getConnection(DB_URL, USER, PASS);
            }
        } catch (Exception ex) {
            Logger.getLogger(Database.class.getName()).log(Level.SEVERE, null, ex);
        }
       
    }
    
    public ResultSet getResultSet(String sql) throws Exception {
        stmt = conn.createStatement();
        return stmt.executeQuery(sql);
    }
    
    protected abstract T getRow(ResultSet rs) throws Exception;
    
    
    public List<T> getList(String sql) throws Exception{
        ResultSet rs = getResultSet(sql);
        List<T> result = new ArrayList<T>();
        while(rs.next()){
            result.add(getRow(rs));
        }
        return result;
    }
}
